<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\MasterKomplain;

/**
 * MasterKomplainSearch represents the model behind the search form about `backend\models\MasterKomplain`.
 */
class MasterKomplainSearch extends MasterKomplain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_master_kom'], 'integer'],
            [['nama_komplain', 'jenis_komplain', 'keterangan', 'standar_waktu'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterKomplain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_master_kom' => $this->id_master_kom,
            'standar_waktu' => $this->standar_waktu,
        ]);

        $query->andFilterWhere(['like', 'nama_komplain', $this->nama_komplain])
            ->andFilterWhere(['like', 'jenis_komplain', $this->jenis_komplain])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}

<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "master_komplain".
 *
 * @property integer $id_master_kom
 * @property string $nama_komplain
 * @property string $jenis_komplain
 * @property string $keterangan
 * @property string $standar_waktu
 */
class MasterKomplain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'master_komplain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_komplain', 'jenis_komplain'], 'required'],
            [['jenis_komplain', 'keterangan'], 'string'],
            [['standar_waktu'], 'safe'],
            [['nama_komplain'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_master_kom' => 'Id Master Kom',
            'nama_komplain' => 'Nama Komplain',
            'jenis_komplain' => 'Jenis Komplain',
            'keterangan' => 'Keterangan',
            'standar_waktu' => 'Standar Waktu',
        ];
    }
    public function getKomplain()
    {
      return $this->hasMany(Komplain::className(), ['master_komplain_id_mk' => 'id_master_kom']);
    }
    public function getSop()
    {
      return $this->hasMany(Sop::className(), ['master_komplain_id_mk' => 'id_master_kom']);
    }
}

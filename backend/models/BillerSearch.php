<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Biller;

/**
 * BillerSearch represents the model behind the search form about `backend\models\Biller`.
 */
class BillerSearch extends Biller
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_biller'], 'integer'],
            [['nama_biller', 'fitur', 'email', 'no_kontak'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Biller::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_biller' => $this->id_biller,
        ]);

        $query->andFilterWhere(['like', 'nama_biller', $this->nama_biller])
            ->andFilterWhere(['like', 'fitur', $this->fitur])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'no_kontak', $this->no_kontak]);

        return $dataProvider;
    }
}

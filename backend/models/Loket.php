<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "loket".
 *
 * @property integer $id_loket
 * @property string $nama_loket
 * @property string $penanggung_jawab
 * @property string $email
 * @property string $no_kontak
 * @property string $alamat
 */
class Loket extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_loket', 'penanggung_jawab', 'email', 'no_kontak', 'alamat'], 'required'],
            [['nama_loket', 'penanggung_jawab', 'email', 'no_kontak', 'alamat'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_loket' => 'Id Loket',
            'nama_loket' => 'Nama Loket',
            'penanggung_jawab' => 'Penanggung Jawab',
            'email' => 'Email',
            'no_kontak' => 'No Kontak',
            'alamat' => 'Alamat',
        ];
    }

    public function getBlokir()
    {
      return $this->hasMany(Blokir::className(), ['loket_id_lok' => 'id_loket']);
    }

    public function getKomplain()
    {
      return $this->hasMany(Komplain::className(), ['loket_id_lok' => 'id_loket']);
    }
}

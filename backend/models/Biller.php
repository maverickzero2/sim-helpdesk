<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "biller".
 *
 * @property integer $id_biller
 * @property string $nama_biller
 * @property string $fitur
 * @property string $email
 * @property string $no_kontak
 */
class Biller extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'biller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama_biller', 'fitur', 'email', 'no_kontak'], 'required'],
            [['nama_biller', 'fitur', 'no_kontak'], 'string', 'max' => 255],
            [['email'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_biller' => 'Id Biller',
            'nama_biller' => 'Nama Biller',
            'fitur' => 'Fitur',
            'email' => 'Email',
            'no_kontak' => 'No Kontak',
        ];
    }


    public function getBlokir()
    {
      return $this->hasMany(Blokir::className(),['biller_id_bil' => 'id_biller']);
    }
}

<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Loket;

/**
 * LoketSearch represents the model behind the search form about `backend\models\Loket`.
 */
class LoketSearch extends Loket
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_loket'], 'integer'],
            [['nama_loket', 'penanggung_jawab', 'email', 'no_kontak', 'alamat'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Loket::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_loket' => $this->id_loket,
        ]);

        $query->andFilterWhere(['like', 'nama_loket', $this->nama_loket])
            ->andFilterWhere(['like', 'penanggung_jawab', $this->penanggung_jawab])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'no_kontak', $this->no_kontak])
            ->andFilterWhere(['like', 'alamat', $this->alamat]);

        return $dataProvider;
    }
}

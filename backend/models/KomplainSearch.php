<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Komplain;

/**
 * KomplainSearch represents the model behind the search form about `backend\models\Komplain`.
 */
class KomplainSearch extends Komplain
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_komplain', 'loket_id_lok', 'master_komplain_id_mk', 'user_id_usr'], 'integer'],
            [['catatan', 'tanggal_waktu_datang', 'waktu_pengerjaaan', 'waktu_selesai', 'solusi_penanganan', 'lama_pengerjaan', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Komplain::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_komplain' => $this->id_komplain,
            'loket_id_lok' => $this->loket_id_lok,
            'master_komplain_id_mk' => $this->master_komplain_id_mk,
            'user_id_usr' => $this->user_id_usr,
            'tanggal_waktu_datang' => $this->tanggal_waktu_datang,
            'waktu_pengerjaaan' => $this->waktu_pengerjaaan,
            'waktu_selesai' => $this->waktu_selesai,
            'lama_pengerjaan' => $this->lama_pengerjaan,
        ]);

        $query->andFilterWhere(['like', 'catatan', $this->catatan])
            ->andFilterWhere(['like', 'solusi_penanganan', $this->solusi_penanganan])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}

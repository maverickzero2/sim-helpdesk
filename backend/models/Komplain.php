<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "komplain".
 *
 * @property integer $id_komplain
 * @property integer $loket_id_lok
 * @property integer $master_komplain_id_mk
 * @property integer $user_id_usr
 * @property string $catatan
 * @property string $tanggal_waktu_datang
 * @property string $waktu_pengerjaaan
 * @property string $waktu_selesai
 * @property string $solusi_penanganan
 * @property string $lama_pengerjaan
 * @property string $status
 */
class Komplain extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'komplain';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['loket_id_lok', 'master_komplain_id_mk', 'user_id_usr', 'catatan', 'tanggal_waktu_datang'], 'required'],
            [['loket_id_lok', 'master_komplain_id_mk', 'user_id_usr'], 'integer'],
            [['tanggal_waktu_datang', 'waktu_pengerjaaan', 'waktu_selesai', 'lama_pengerjaan'], 'safe'],
            [['solusi_penanganan', 'status'], 'string'],
            [['catatan'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_komplain' => 'Id Komplain',
            'loket_id_lok' => 'Loket Id Lok',
            'master_komplain_id_mk' => 'Master Komplain Id Mk',
            'user_id_usr' => 'User Id Usr',
            'catatan' => 'Catatan',
            'tanggal_waktu_datang' => 'Tanggal Waktu Datang',
            'waktu_pengerjaaan' => 'Waktu Pengerjaaan',
            'waktu_selesai' => 'Waktu Selesai',
            'solusi_penanganan' => 'Solusi Penanganan',
            'lama_pengerjaan' => 'Lama Pengerjaan',
            'status' => 'Status',
        ];
    }
    public function getLoket()
    {
      return $this->hasOne(Loket::className(), ['id_loket' => 'loket_id_lok']);
    }

    public function getMasterKomplain()
    {
      return $this->hasOne(MasterKomplain::className(), ['id_master_kom' => 'master_komplain_id_mk']);
    }

    public function getUser()
    {
      return $this->hasOne(User::className(), ['id' => 'user_id_usr']);
    }
}

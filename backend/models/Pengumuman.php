<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "pengumuman".
 *
 * @property integer $id_pengumuman
 * @property string $judul
 * @property string $tanggal_waktu
 * @property string $isi
 */
class Pengumuman extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengumuman';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['judul', 'tanggal_waktu', 'isi'], 'required'],
            [['tanggal_waktu'], 'safe'],
            [['isi'], 'string'],
            [['judul'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_pengumuman' => 'Id Pengumuman',
            'judul' => 'Judul',
            'tanggal_waktu' => 'Tanggal Waktu',
            'isi' => 'Isi',
        ];
    }
}

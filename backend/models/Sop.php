<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "sop".
 *
 * @property integer $id_sop
 * @property integer $master_komplain_id_mk
 * @property string $tanggal_waktu
 * @property string $isi
 */
class Sop extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sop';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['master_komplain_id_mk', 'tanggal_waktu', 'isi'], 'required'],
            [['master_komplain_id_mk'], 'integer'],
            [['tanggal_waktu'], 'safe'],
            [['isi'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_sop' => 'Id Sop',
            'master_komplain_id_mk' => 'Master Komplain Id Mk',
            'tanggal_waktu' => 'Tanggal Waktu',
            'isi' => 'Isi',
        ];
    }
    public function getMasterKomplain()
    {
      return $this->hasOne(MasterKomplain::className(), ['id_master_kom' => 'master_komplain_id_mk']);
    }
}

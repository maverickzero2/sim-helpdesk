<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $username
 * @property string $jabatan
 * @property string $pengerjaan_terakhir
 * @property string $status_pengerjaan
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'username', 'jabatan', 'pengerjaan_terakhir', 'status_pengerjaan', 'auth_key', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['jabatan', 'status_pengerjaan'], 'string'],
            [['pengerjaan_terakhir'], 'safe'],
            [['role', 'status', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 100],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'username' => 'Username',
            'jabatan' => 'Jabatan',
            'pengerjaan_terakhir' => 'Pengerjaan Terakhir',
            'status_pengerjaan' => 'Status Pengerjaan',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getKomplain()
    {
      return $this->hasMany(Komplain::className(), ['user_id_usr' => 'id']);
    }
}

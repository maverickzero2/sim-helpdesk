<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\DashboardAsset;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

DashboardAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-green sidebar-mini">
<?php $this->beginBody() ?>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>S</b>HD</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SIM</b>HD</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs"><?= Yii::$app->user->identity->username; ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p><?= Yii::$app->user->identity->username; ?></p>
              </li>
              <!-- Menu Body -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                <?= Html::a(
                                    'Profile',
                                    ['site/profile'],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                </div>
                <div class="pull-right">
                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= Yii::$app->user->identity->username; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="index.php">
            <i class="fa fa-dashboard"></i> <span>Home</span>
          </a>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Forms</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="index.php?r=biller%2Fcreate"><i class="fa fa-circle-o"></i> Tambah biller</a></li>
            <li><a href="index.php?r=blokir%2Fcreate"><i class="fa fa-circle-o"></i> Permintaan blokir</a></li>
            <li><a href="index.php?r=komplain%2Fcreate"><i class="fa fa-circle-o"></i> Tambah komplain</a></li>
            <li><a href="index.php?r=loket%2Fcreate"><i class="fa fa-circle-o"></i> Tambah loket</a></li>
            <li><a href="index.php?r=master-komplain%2Fcreate"><i class="fa fa-circle-o"></i> Tambah master komplain</a></li>
            <li><a href="index.php?r=pengumuman%2Fcreate"><i class="fa fa-circle-o"></i> Tambah pengumuman</a></li>
            <li><a href="index.php?r=sop%2Fcreate"><i class="fa fa-circle-o"></i> Tambah sop</a></li>
            <li><a href="index.php?r=user%2Fcreate"><i class="fa fa-circle-o"></i> Tambah user</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">

            <li><a href="index.php?r=biller"><i class="fa fa-circle-o"></i> Data biller</a></li>
            <li><a href="index.php?r=blokir"><i class="fa fa-circle-o"></i> Data blokir</a></li>
            <li><a href="index.php?r=komplain"><i class="fa fa-circle-o"></i> Data komplain</a></li>
            <li><a href="index.php?r=loket"><i class="fa fa-circle-o"></i> Data loket</a></li>
            <li><a href="index.php?r=master-komplain"><i class="fa fa-circle-o"></i> Data master komplain</a></li>
            <li><a href="index.php?r=pengumuman"><i class="fa fa-circle-o"></i> Data pengumuman</a></li>
            <li><a href="index.php?r=sop"><i class="fa fa-circle-o"></i> Data sop</a></li>
            <li><a href="index.php?r=user"><i class="fa fa-circle-o"></i> Data user</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
      <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= Alert::widget() ?>
        <?= $content ?>
        </section>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

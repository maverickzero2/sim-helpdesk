<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MasterKomplainSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-komplain-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_master_kom') ?>

    <?= $form->field($model, 'nama_komplain') ?>

    <?= $form->field($model, 'jenis_komplain') ?>

    <?= $form->field($model, 'keterangan') ?>

    <?= $form->field($model, 'standar_waktu') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

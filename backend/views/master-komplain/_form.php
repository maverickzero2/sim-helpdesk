<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\MasterKomplain */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="master-komplain-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_komplain')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_komplain')->dropDownList([ 'Aplikasi Bermasalah' => 'Aplikasi Bermasalah', 'Transaksi Bermasalah' => 'Transaksi Bermasalah', ], ['prompt' => '']) ?>

    <?= $form->field($model, 'keterangan')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'standar_waktu')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

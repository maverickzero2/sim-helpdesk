<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\BillerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Biller';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biller-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Biller', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id_biller',
            'nama_biller',
            'fitur',
            'email:email',
            'no_kontak',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

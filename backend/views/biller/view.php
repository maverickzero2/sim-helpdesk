<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Biller */

$this->title = $model->id_biller;
$this->params['breadcrumbs'][] = ['label' => 'Billers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biller-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_biller], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_biller], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_biller',
            'nama_biller',
            'fitur',
            'email:email',
            'no_kontak',
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Biller */

$this->title = 'Update Biller: ' . $model->id_biller;
$this->params['breadcrumbs'][] = ['label' => 'Billers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_biller, 'url' => ['view', 'id' => $model->id_biller]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="biller-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

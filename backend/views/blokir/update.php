<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Blokir */

$this->title = 'Update Blokir: ' . $model->id_blokir;
$this->params['breadcrumbs'][] = ['label' => 'Blokirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_blokir, 'url' => ['view', 'id' => $model->id_blokir]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="blokir-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

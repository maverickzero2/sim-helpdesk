<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Blokir */

$this->title = 'Permintaan Blokir';
$this->params['breadcrumbs'][] = ['label' => 'Blokirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blokir-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

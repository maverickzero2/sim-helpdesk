<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Biller;
use backend\models\Loket;

/* @var $this yii\web\View */
/* @var $model backend\models\Blokir */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="blokir-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'loket_id_lok')->dropDownList(
            ArrayHelper::map(Loket::find()->all(),'id_loket','nama_loket'),
            ['promt'=>'Pilih loket']
      ) ?>

    <?= $form->field($model, 'biller_id_bil')->dropDownList(
            ArrayHelper::map(Biller::find()->all(),'id_biller','nama_biller'),
            ['promt'=>'Pilih biller']
      ) ?>



    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Tambah' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KomplainSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Komplain';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="komplain-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Tambah Komplain', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'loket.nama_loket',
            'masterKomplain.nama_komplain',
            'user.username',
            'catatan',
            'tanggal_waktu_datang',
            // 'waktu_pengerjaaan',
            // 'waktu_selesai',
            // 'solusi_penanganan:ntext',
            // 'lama_pengerjaan',
            // 'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Loket */

$this->title = 'Tambah Loket';
$this->params['breadcrumbs'][] = ['label' => 'Lokets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loket-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

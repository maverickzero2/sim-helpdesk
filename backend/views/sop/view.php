<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Sop */

$this->title = $model->id_sop;
$this->params['breadcrumbs'][] = ['label' => 'Sops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sop-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_sop], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_sop], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_sop',
            'master_komplain_id_mk',
            'tanggal_waktu',
            'isi:ntext',
        ],
    ]) ?>

</div>

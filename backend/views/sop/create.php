<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Sop */

$this->title = 'Tambah Sop';
$this->params['breadcrumbs'][] = ['label' => 'Sops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sop-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
